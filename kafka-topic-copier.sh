#!/bin/bash

HELP=true
RUN=false
KILL=false
CLEAN=false
VERIFY=false
ORDER=false
DRYRUN=""
ZOOKEEPER=$(cat /etc/kafka/server.properties | \
  grep zookeeper.connect= | cut -f2 -d'=')
BROKERS=localhost:9092
REGISTRY=http://localhost:8081

ENV=$(hostname -f | cut -f2 -d'.')
if [ "$ENV" != "preprod" ]; then ENV=prod; fi
IMAGE=nexus.$ENV.makeorg.tech/kafka-topic-copier

SH=""
if ! which kafka-run-class >/dev/null 2>&1; then
  SH=".sh"
fi

# config = [dest topic (without -v2)] = "source topic","Xmx value"
declare -A config=(
  ["emails"]="emails,512M"
  ["mailjet"]="mailjet-events,512M"
  ["crm-contact"]="crm-contact,512M"
  ["proposals"]="proposals,4G"
  ["users"]="users,512M"
  ["duplicates"]="proposal-duplicates-predicted,512M"
  ["tracking"]="external-tracking-events,4G"
  ["ideas"]="ideas,512M"
  ["users-update"]="users-update,512M")

topics=("${!config[@]}")

usage() {
  cat <<USAGE
$0 [actions] [options]
  -h|--help             show help
  -r|--run              run kafka-topic-copier for one or all topics
  -k|--kill             kill one or more instances
  -c|--clean            remove all temporary and destination topic, clean group
  -v|--verify           check the state of topics, groups and more
  -o|--checkorder       check order on each partition (can take a long time)
  -d|--dryrun           do not really do it, just show what will be done
  -t|--topic [topic]    execute action(s) only for specified destination topic
  -z|--zookeeper hosts  connection string for the zookeeper connection
  -b|--brokers          brokers connection string
  -s|--sregistry        schema-registry connection string
USAGE
}

while [[ $# -gt 0 ]]; do
  key="$1"
  case $key in
    -h|--help)
      HELP=true
      shift
      ;;
    -r|--run)
      RUN=true
      HELP=false
      shift
      ;;
    -k|--kill)
      KILL=true
      HELP=false
      shift
      ;;
    -c|--clean)
      CLEAN=true
      HELP=false
      shift
      ;;
    -v|--verify)
      VERIFY=true
      HELP=false
      shift
      ;;
    -o|--checkorder)
      ORDER=true
      HELP=false
      shift
      ;;
    -d|--dryrun)
      DRYRUN=echo
      DRYRUN2='xargs -I % echo % |'
      shift
      ;;
    -t|--topic)
      topics=("$2")
      shift
      shift
      ;;
    -z|--zookeeper)
      ZOOKEEPER=("$2")
      shift
      shift
      ;;
    -b|--brokers)
      BROKERS=("$2")
      shift
      shift
      ;;
    -s|--sregistry)
      REGISTRY=("$2")
      shift
      shift
      ;;
    -i|--image)
      IMAGE=("$2")
      shift
      shift
      ;;
    *)
      echo -e "Invalid option!\n"
      usage
      exit 1
  esac
done

clean_topic() {
  local target=$1
  echo "==> Delete $target and tmp-$target topics"
  for topic in $target tmp-$target; do
    $DRYRUN kafka-topics$SH --zookeeper $ZOOKEEPER --delete --topic $topic
  done
  echo "==> Delete $target-value and tmp-$target-value subjects"
  for topic in $target tmp-$target; do
    $DRYRUN curl -X DELETE $REGISTRY/subjects/$topic-value
  done
}

clean_group() {
  local target=$1
  echo "==> Delete kafka-topic-copier-$target group"
  $DRYRUN kafka-consumer-groups$SH --bootstrap-server $BROKERS \
    --delete --group kafka-topic-copier-$target
}

run() {
  source=$1
  dest=$2
  xmx=$3
  echo "==> Run Kafka-Topic-Copier for $source => $dest"
  $DRYRUN docker run -d --name kafka-topic-copier-$dest --net=host \
    -e JAVA_OPTS=-Xmx$xmx \
    -e BOOTSTRAP_SERVERS=$BROKERS \
    -e REGISTRY_URL=$REGISTRY \
    -e SOURCE_TOPIC=$source -e DESTINATION_TOPIC=$dest \
    -e APPLICATION_ID=kafka-topic-copier-$dest \
    -e THREADS=6 \
    -e RANGE=2000000 \
    $IMAGE
}

kill_copier() {
  dest=$1
  echo "==> Kill kafka-topic-copier-$dest"
  $DRYRUN docker kill --signal=15 kafka-topic-copier-$dest
}

rm_docker() {
  dest=$1
  echo "==> Remove kafka-topic-copier-$dest"
  $DRYRUN docker rm kafka-topic-copier-$dest
}

show_offsets() {
  source=$1
  dest=$2
  echo "==> Offsets: $source => $dest"
  $DRYRUN kafka-run-class$SH kafka.tools.GetOffsetShell \
    --broker-list $BROKERS --topic $source --time -1 | \
    $DRYRUN2 awk -F ':' '{ SUM += $3; print $0} END { print SUM }'
  $DRYRUN kafka-run-class$SH kafka.tools.GetOffsetShell \
    --broker-list $BROKERS --topic tmp-$dest --time -1 | \
    $DRYRUN2 awk -F ':' '{ SUM += $3; print $0} END { print SUM }'
  $DRYRUN kafka-run-class$SH kafka.tools.GetOffsetShell \
    --broker-list $BROKERS --topic $dest --time -1 | \
    $DRYRUN2 awk -F ':' '{ SUM += $3; print $0} END { print SUM }'
}

show_logs() {
  dest=$1
  $DRYRUN docker logs kafka-topic-copier-$dest | $DRYRUN2 tail -n 3
}

show_subjects() {
  dest=$1
  echo "==> Subjects for $dest-value"
  $DRYRUN curl -s $REGISTRY/subjects/$dest-value/versions
}

check_order() {
  dest=$1
  echo $dest
  for p in $(seq 0 2); do
    $DRYRUN docker run -ti --rm --net=host --log-driver none confluentinc/cp-kafkacat \
      kafkacat -b localhost:9092 -C -t $dest -o 0 -p $p -e \
      -X enable.auto.commit=false -f '%T %o\n' | $DRYRUN2 \
      awk 'BEGIN{last=0;i=0;maxdiff=0}
           {
             a=int($1);
             lasta=int(last);
             if(a<lasta&&a!=0){
               i++;
               maxdiff=lasta-a
             };
             last=$1
           }
           END{printf("num:%d max:%d\n",i,maxdiff)}'
  done
}

if [ "$HELP" = "true" ]; then
  usage
  exit 0
fi


if [ "$KILL" = "true" ]; then
  for i in ${topics[*]}; do
    dest="$i-v2"
    kill_copier $dest
  done
  $DRYRUN sleep 5
  for i in ${topics[*]}; do
    dest="$i-v2"
    rm_docker $dest
  done
  echo ""
fi

if [ "$CLEAN" = "true" ]; then
  for i in ${topics[*]}; do
    dest="$i-v2"
    clean_topic $dest
  done
  $DRYRUN sleep 5
  for i in ${topics[*]}; do
    dest="$i-v2"
    clean_group $dest
  done
  echo ""
fi

if [ "$RUN" = "true" ]; then
  echo "==> Pull latest image: $IMAGE"
  $DRYRUN docker pull $IMAGE
  $DRYRUN sudo docker pull $IMAGE
  for i in ${topics[*]}; do
    dest="$i-v2"
    source=$(echo "${config["$i"]}" | cut -f1 -d',')
    xmx=$(echo "${config["$i"]}" | cut -f2 -d',')
    run $source $dest $xmx
  done
fi

if [ "$VERIFY" = "true" ]; then
  for i in ${topics[*]}; do
    dest="$i-v2"
    source=$(echo "${config["$i"]}" | cut -f1 -d',')
    show_offsets $source $dest
    show_logs $dest
    show_subjects $dest
    echo -e "\n"
  done
fi

if [ "$ORDER" = "true" ]; then
  for i in ${topics[*]}; do
    dest="$i-v2"
    check_order $dest
  done
fi
