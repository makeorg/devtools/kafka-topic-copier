package org.make.kafka.copier

import java.time.ZonedDateTime

import org.apache.avro.generic.GenericRecord
import org.apache.avro.util.Utf8
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.streams.processor.TimestampExtractor

class AvroTimestampExtractor extends TimestampExtractor {
  override def extract(record: ConsumerRecord[AnyRef, AnyRef],
                       previousTimestamp: Long): Long = {

    (record.key(), record.value()) match {
      case (_, value: GenericRecord) =>
        AvroTimestampExtractor.extractTimestamp(value).getOrElse(record.timestamp())

      case _ => record.timestamp()
    }
  }
}

object AvroTimestampExtractor {
  def extractTimestamp(value: GenericRecord): Option[Long] = {
    Option(value.get("date"))
      .map(_.asInstanceOf[Utf8])
      .map(ZonedDateTime.parse)
      .map(_.toInstant.toEpochMilli)
  }
}
