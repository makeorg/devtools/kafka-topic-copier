
organization := "org.make"
name := "kafka-topic-copier"

scalaVersion := "2.12.7"

val kafkaStreamsVersion: String = "2.1.0"

libraryDependencies ++= Seq(
  "org.apache.kafka" % "kafka-streams" % kafkaStreamsVersion,
  "org.apache.kafka" % "kafka-clients" % kafkaStreamsVersion,
  "org.apache.kafka" %% "kafka-streams-scala" % kafkaStreamsVersion,
  "org.apache.avro" % "avro" % "1.8.2",
  "io.confluent" % "kafka-avro-serializer" % "5.0.1",
  "com.typesafe" % "config" % "1.3.2",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.0",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.lihaoyi" %% "pprint" % "0.5.3"
)

resolvers += "Sonatype Nexus Repository Manager".at("https://nexus.prod.makeorg.tech/repository/maven-public/")

git.formattedShaVersion := git.gitHeadCommit.value.map(_.take(10))

ThisBuild / version := {
  git.formattedShaVersion.value.get
}
