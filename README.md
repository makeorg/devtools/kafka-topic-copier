kafka-topic-copier
==================

This program will read a kafka topic, using avro with a registry to decode
content, and will write the result in another topic.

It handles re-partionning, using the `id` key in the avro message and a change
in the timestamps, using the `date` field in the messages.

It will also give null id to every message. The previous key in the kafka
messages is expected to be a `String`.

Its output will be sorted by timestamp.

Starting the program using docker
---------------------------------

The image name is `nexus.prod.makeorg.tech/kafka-topic-copier`.

The version is either `latest` or the 10 first characters of the needed git
commit.

Environment variables used for configuration are listed in
[this file](src/main/resources/application.conf).

Example:
```
  docker run -ti --rm --name kafka-topic-copier --net=host \
    -e SOURCE_TOPIC=topic-v1 \
    -e DESTINATION_TOPIC=topic-v2 \
    -e TEMPORARY_TOPIC=tmp-vtmp \
    -e RANGE=100000 -e THREADS=6 \
    nexus.preprod.makeorg.tech/kafka-topic-copier
```

Helper script
-------------

Get [kafka-topic-copier.sh](kafka-topic-copier.sh) and modify `config` hash.
